<?php

return [
    
    'email' => ['contacto@madererapizarro.com'],
    
    'phones' => [
        '(+51) 01 283-3693',
        '(+51) 9950 28229',
        '(+51) 9901 90191'
    ],
    
    //Activar redes sociales
    //'facebook' => 'http://facebook.com/MadereraPizarro',
    //'twitter' => 'http://twitter.com/MadereraPizarro',
    
    
    //Verificar que las imágenes existan en la carpeta images/
    //Recomendado el uso de imágenes de 1170x400 y que no pesen mucho
    'productos' => [
        'capirona' => [
            'name' => 'Capirona',
            'image' => 'maderas/capirona.jpg',
            'icon' => 'maderas/icons/capirona.jpg',
            'slide' => 'maderas/slides/capirona.jpg',
            'texture' => 'maderas/texturas/capirona.jpg',
            'description' => 'descripción de madera capirona',
            'details' => [
                'Nombres' => [
                    'Nombre en inglés' => 'Capirona',
                    'Sinónimos internacionales' => 'Pau mulato',
                    'Nombre científico' => 'Calycophyllum spruceanum',
                    'Nombres comunes' => 'Capirona, Pau Mulato',
                    'Familia' => 'Rubiaceae',
                    'Origen' => 'Tropical S/A',
                ],
                'Descripción de la madera' => [
                    'Color' => 'blanco pardo',
                    'Grano' => 'recto a entrecruzado',
                    'Textura' => 'muy fina',
                    'Brillo' => 'medio intenso',
                    'Vetado' => 'poco diferenciado'
                ],
                'Características de procesamiento y uso' => [
                    'Comportamiento al secado artificial' => 'bueno',
                    'Durabilidad natural' => 'alta',
                    'Trabajabilidad' => 'bueno',
                    'Uso' => 'vigas, columnas, pisos, mueblería molduras, artesanías, mangos de herramientas entre otros',
                ],
                'Propiedades físicas y mecánicas' => [
                    'Densidad básica' => '760 kg/m3'
                ]
            ]
        ],
        'cashimbo' => [
            'name' => 'Cashimbo',
            'image' => 'maderas/cashimbo.jpg',
            'slide' => 'maderas/slides/cashimbo.jpg',
            'texture' => 'maderas/texturas/cashimbo.jpg',
            'icon' => 'maderas/icons/cashimbo.jpg',
            'description' => 'descripción de madera cashimbo',
            'details' => [
                'Nombres' => [
                    'Nombre en inglés' => 'Jekitiba',
                    'Sinónimos internacionales' => 'Bacú',
                    'Nombre científico' => 'Cariniana spp.',
                    'Nombres comunes' => 'Cashimbo Cachimbo',
                    'Familia' => 'Lecythidaceae',
                    'Origen' => 'Tropical S/A',
                ],
                'Descripción de la madera' => [
                    'Color' => 'Rosado cremoso',
                    'Grano' => 'de recto a entrecruzado',
                    'Textura' => 'media',
                    'Brillo' => 'bajo a mediano',
                    'Vetado' => 'Satinado poco pronunciado',
                ],
                'Características de procesamiento y uso' => [
                    'Comportamiento al secado artificial' => 'bueno',
                    'Durabilidad natural' => 'mediana',
                    'Trabajabilidad' => 'bueno',
                    'Uso' => 'Carpinteria interior, contra chapado, molduras, construcción ligera, mobiliario corriente, embalaje, juguetes, entarimados',
                ],
                'Propiedades físicas y mecánicas' => [
                    'Densidad básica' => '590 kg/m3'
                ]
            ]
        ],
        'catahua' => [
            'name' => 'Catahua',
            'image' => 'maderas/catahua.jpg',
            'icon' => 'maderas/icons/catahua.jpg',
            'texture' => 'maderas/texturas/catahua.jpg',
            'description' => 'descripción de madera catahua',
            'details' => [
                'Nombres' => [
                    'Nombre en inglés' => 'Assacu',
                    'Sinónimos internacionales' => 'Mura, Acacu, Possum',
                    'Nombre científico' => 'Hura crepitans L.',
                    'Nombres comunes' => 'Catahua',
                    'Familia' => 'Euphorbiaceae',
                    'Origen' => 'Tropical S/A',
                ],
                'Descripción de la madera' => [
                    'Color' => 'blanca amarillenta',
                    'Grano' => 'recto a entrecruzado',
                    'Textura' => 'fina',
                    'Brillo' => 'Alto',
                    'Vetado' => 'poco diferenciado',
                ],
                'Características de procesamiento y uso' => [
                    'Comportamiento al secado artificial' => 'bueno',
                    'Durabilidad natural' => 'moderado',
                    'Trabajabilidad' => 'bueno',
                    'Uso' => 'Cajoneria , revestimiento interior, chapas y contrachapado, material de relleno, muebleria barata y ebanisteria, embalaje de productos prerecibles, triplay donde se requiere una madera liviana y facil de trabajar.',
                ],
                'Propiedades físicas y mecánicas' => [
                    'Densidad básica' => '0.41 gr/cm3'
                ]
            ]
        ],
        'copayba' => [
            'name' => 'Copayba',
            'image' => 'maderas/copayba.jpg',
            'icon' => 'maderas/icons/copayba.jpg',
            'texture' => 'maderas/texturas/copayba.jpg',
            'description' => 'descripción de madera copayba',
            'details' => [
                'Nombres' => [
                    'Nombre en inglés' => 'Copaiba',
                    'Sinónimos internacionales' => 'Copaifera jacquini Desf; Copaiba officinalis Adans. ; Copaiva officinalis Jacq',
                    'Nombre científico' => 'Copaifera officinalisL.',
                    'Nombres comunes' => 'Copaiba, aceite cabimo, cabima',
                    'Familia' => 'Caesalpinioideae',
                    'Origen' => 'Tropical S/A',
                ],
                'Descripción de la madera' => [
                    'Color' => ' blanco rosáceo',
                    'Grano' => 'Recto',
                    'Textura' => 'Media  a fina',
                    'Brillo' => 'Medio',
                    'Vetado' => 'Arcos superpuestos y bandas longitudinales muy angostas y oscuras',
                ],
                'Características de procesamiento y uso' => [
                    'Comportamiento al secado artificial' => 'bueno',
                    'Durabilidad natural' => 'alta',
                    'Trabajabilidad' => 'bueno',
                    'Uso' => 'Vigas, columnas, machihembrados, muebles y objetos torneados. También se utiliza en carpintería, pisos , revestimiento interiores, parquet, contra chapado, entarimado,  elaboración de cajas, molduras, encofrados y laminados, por sus cualidades podría sustituir al Pino Oregon. Preservada podria utilizarse para estantillos o postes para cercas. Es apta para tableros de partículas y tableros madera - cemento.',
                ],
                'Propiedades físicas y mecánicas' => [
                    'Densidad básica' => '0.61 gr/cm3'
                ]
            ]
        ],
        'huayruro' => [
            'name' => 'Huayruro',
            'image' => 'maderas/huayruro.jpg',
            'icon' => 'maderas/icons/huayruro.jpg',
            'texture' => 'maderas/texturas/huayruro.jpg',
            'description' => 'descripción de madera huayruro',
            'details' => [
                'Nombres' => [
                    'Nombre en inglés' => 'Angelim pedra',
                    'Sinónimos internacionales' => '',
                    'Nombre científico' => 'Ormosia macrocarpia',
                    'Nombres comunes' => 'Angelim pedra',
                    'Familia' => 'Fabaceae leguminosae',
                    'Origen' => 'Tropical S/A',
                ],
                'Descripción de la madera' => [
                    'Color' => 'marrón claro, y amarillo a rojizo',
                    'Grano' => 'entrecruzado',
                    'Textura' => 'gruesa',
                    'Brillo' => 'medio',
                    'Vetado' => 'arcos superpuestos en corte tangencial y bandas paralelas',
                    'Olor' => 'poco perceptible',
                ],
                'Características de procesamiento y uso' => [
                    'Comportamiento al secado artificial' => 'bueno',
                    'Durabilidad natural' => 'media - alta',
                    'Trabajabilidad' => 'muy bueno',
                    'Uso' => 'estructuras como vigas, columnas, tijerales, mueblería, pisos, marco de puertas y ventanas, entre otros',
                ],
                'Propiedades físicas y mecánicas' => [
                    'Densidad básica' => '610 kg/m3 medio - alto'
                ]
            ]
        ],
        'roble' => [
            'name' => 'Roble',
            'image' => 'maderas/roble.jpg',
            'icon' => 'maderas/icons/roble.jpg',
            'texture' => 'maderas/texturas/roble.jpg',
            'description' => 'descripción de madera roble',
            'details' => [
                'Nombres' => [
                    'Nombre en inglés' => 'Cerejeira',
                    'Sinónimos internacionales' => 'Roble Boliviano, Ishpingo',
                    'Nombre científico' => 'Amburana cearensis',
                    'Nombres comunes' => 'Roble boliviano, Cerejeira',
                    'Familia' => 'Fabaceae',
                    'Origen' => 'Tropical S/A',
                ],
                'Descripción de la madera' => [
                    'Color' => 'amarillo a marrón',
                    'Grano' => 'entrecruzado',
                    'Textura' => 'media a gruesa',
                    'Brillo' => 'medio',
                    'Vetado' => 'arcos superpuestos y bandas',
                    'Olor' => 'característico',
                ],
                'Características de procesamiento y uso' => [
                    'Comportamiento al secado artificial' => 'muy bueno',
                    'Durabilidad natural' => 'media - alta',
                    'Trabajabilidad' => 'muy buena',
                    'Uso' => 'mueblería en general, laminados, puertas, ventanas y otros',
                ],
                'Propiedades físicas y mecánicas' => [
                    'Densidad básica' => '430 kg/m3 media'
                ]
            ]
        ],
        'tornillo' => [
            'name' => 'Tornillo',
            'image' => 'maderas/tornillo.jpg',
            'icon' => 'maderas/icons/tornillo.jpg',
            'texture' => 'maderas/texturas/tornillo.jpg',
            'description' => 'descripción de madera tornillo',
            'details' => [
                'Nombres' => [
                    'Nombre en inglés' => 'Tornillo',
                    'Sinónimos internacionales' => 'Cedro arana',
                    'Nombre científico' => 'Cedrelinga catenaeformis',
                    'Nombres comunes' => 'Tornillo',
                    'Familia' => 'Mimosaceae',
                    'Origen' => 'Tropical S/A',
                ],
                'Descripción de la madera' => [
                    'Color' => 'marrón claro',
                    'Grano' => 'entrecruzada',
                    'Textura' => 'gruesa',
                    'Brillo' => 'medio',
                    'Vetado' => 'poco diferenciado',
                    'Olor' => 'característico',
                ],
                'Características de procesamiento y uso' => [
                    'Comportamiento al secado artificial' => 'muy bueno',
                    'Durabilidad natural' => 'alta',
                    'Trabajabilidad' => 'muy buena',
                    'Uso' => 'estructural, como vigas columnas, tijerales y carpinterí­a en general',
                ],
                'Propiedades físicas y mecánicas' => [
                    'Densidad básica' => '450 kg/m3 medio'
                ]
            ]
        ],
    ],
    
    //Se mostrarán solo los 3 primeros servicios
    'servicios' => [
        'corte' => [
            'shortname' => 'Cortes',
            'name' => 'Corte o Tableado de Madera',
            'description' => 'Maderera Pizarro ofrece diversos servicios para el tratado de la madera y/o producto...',
            'image' => 'servicios1.jpg',
        ],
        'cepillado' => [
            'shortname' => 'Cepillado',
            'name' => 'Cepillado de Madera',
            'description' => 'Además del tableado de maderas, Maderera Pizarro ofrece el servicio de cepillado...',
            'image' => 'servicios2_1.jpg',
        ],
        'machihembrado' => [
            'shortname' => 'Machihembrado',
            'name' => 'Machihembrado de Madera',
            'description' => 'El machihembrado es un servicio de ensamble de tablas de madera cepillada...',
            'image' => 'servicios3_1.jpg',
        ],
        'transporte' => [
            'shortname' => 'Transporte',
            'name' => 'Transporte de carga',
            'description' => 'Maderera Pizarro ofrece el servicio de transporte de madera o carga...',
            'image' => 'servicios4.jpg',
        ]
    ],
    
    'news' => [
        [
            'day' => '7',
            'month' => 'JUL',
            'title' => 'Lanzamiento del sitio web MadereraPizarro.com 2015',
            'subtitle' => 'Bienvenidos a la nueva página web',
            'content' => "Descripción de noticia 7 Julio 2015"
        ],
    ],
    
    'menus' => [
        [
            'name' => 'Inicio',
            'url' => '',
        ],
        [
            'name' => 'Nosotros',
            'url' => 'nosotros',
            'submenu' => [
                [
                    'name' => '¿Quiénes somos?',
                    'url' => '#quienes-somos'
                ],
                [
                    'name' => 'Misión y Visión',
                    'url' => '#mision-y-vision'
                ]
            ]
        ],
        [
            'name' => 'Productos',
            'url' => 'productos',
            'submenu' => [
                [
                    'name' => 'Maderas',
                    'url' => '#maderas'
                ]
            ]
        ],
        [
            'name' => 'Maderas',
            'url' => 'productos',
            'submenu' => [
                [
                    'name' => 'Capirona',
                    'url' => '/capirona'
                ],
                [
                    'name' => 'Cashimbo',
                    'url' => '/cashimbo'
                ],
                [
                    'name' => 'Catahua',
                    'url' => '/catahua'
                ],
                [
                    'name' => 'Copayba',
                    'url' => '/copayba'
                ],
                [
                    'name' => 'Huayruro',
                    'url' => '/huayruro'
                ],
                [
                    'name' => 'Roble',
                    'url' => '/roble'
                ],
                [
                    'name' => 'Tornillo',
                    'url' => '/tornillo'
                ],
            ]
        ],
        [
            'name' => 'Servicios',
            'url' => 'servicios',
            'submenu' => [
                [
                    'name' => 'Corte o Tableado de Madera',
                    'url' => '#corte'
                ],
                [
                    'name' => 'Cepillado de Madera',
                    'url' => '#cepillado'
                ],
                [
                    'name' => 'Machihembrado de Madera',
                    'url' => '#machihembrado'
                ],
                [
                    'name' => 'Transporte de Carga',
                    'url' => '#transporte'
                ],
            ]
        ],
        [
            'name' => 'Sostenibilidad',
            'url' => 'sostenibilidad',
        ],
        [
            'name' => 'Contacto',
            'url' => 'contacto',
            'submenu' => [
                [
                    'name' => '¿Dónde nos encontramos?',
                    'url' => '#donde-nos-encontramos'
                ],
                [
                    'name' => 'Envíenos un mensaje',
                    'url' => '#envienos-un-mensaje'
                ]
            ]
        ],
    ],
];