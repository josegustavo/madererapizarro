<?php

return array(
	// required
	'database_type' => 'mysql',
	'database_name' => 'madererapizarro',
	'server' => '127.0.0.1',
	'username' => 'root',
	'password' => '',
 
	// optional
	'port' => 3306,
	'charset' => 'utf8',
	// driver_option for connection, read more from http://www.php.net/manual/en/pdo.setattribute.php
	'option' => array(
		PDO::ATTR_CASE => PDO::CASE_LOWER,
		PDO::ATTR_STRINGIFY_FETCHES => FALSE,
		PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_NUM
	)
);
