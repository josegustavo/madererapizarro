<?php

return [
    'urlRoot' => '/',
    
    'i18n' => [
        'es' => 'Español',
        'en' => 'English'
    ],
    
    'key' => '053M9tfAH7MEaDndGJF4zhrn3m8Z3x5f',
    
    'emailAttach' => [
        'application/vnd.ms-excel', 'application/msexcel', 'application/x-msexcel',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        'application/x-excel', 'application/x-ms-excel', 'application/xls'
    ],
];
