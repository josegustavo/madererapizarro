<?php

class Bootstrap
{
    public function __construct()
    {
        define('START', microtime(true));
        
        //Handle Errors
        $this->handleErrors();
        
        //Register autoload
        $this->autoLoader();
        
        //Load config
        $this->loadConfig();
        
        
        $this->init();        
    }
    
    private function handleErrors()
    {
        Flight::set('flight.log_errors', true);
        
        Flight::map('error', function(Exception $ex, $detail = ""){
            
            $code = 500;
            $message = 'Error interno';
            if($ex->getCode() >= 400 && $ex->getCode() < 500)
            {
                $code = $ex->getCode();
                $message = 'Error en la solicitud';
                if(!$detail) $detail = 'Lo sentimos pero ha ocurrido un error al solicitar la página, vuelva a intentarlo, o intente con otra dirección';
            }
            
            if($ex->getCode() >= 500 && $ex->getCode() < 600)
            {
                $code = $ex->getCode();
            }
            
            if(!$detail) $detail = 'Lo sentimos pero la página que ha especificado tiene un problemas, el cual ya está siendo reportado. gracias por su comprensión';
            
            $error = [
                'title'     => Flight::t('Lo sentimos'),
                'message'   => Flight::t($message),
                'code'      => $code,
                'detail'    => Flight::t($detail),
            ];
            
            if(FLIGHT_DEBUG)
            {
                $error['debug'] = [
                    'trace' => $ex->getTraceAsString(),
                    'code' => $ex->getCode(),
                    'file' => $ex->getFile(),
                    'line' => $ex->getLine(),
                    'message' => utf8_encode($ex->getMessage()),
                ];
            }
            
            if(Flight::request()->ajax)
            {
                
                Flight::json(['error' => $error], $code);
                
                var_dump($error);exit;
            }
            else
            {
                Flight::render('app/error', ['error' => $error]);
                $this->stop($code);
            }
        });

        Flight::map('notFound', function()
        {
            $error = [
                'title'     => Flight::t('Lo sentimos'),
                'message'   => Flight::t('No se encontró la página'),
                'code'      => '404',
                'detail'    => Flight::t('Lo sentimos pero la página que ha especificado no existe o ha sido eliminada')
            ];
            if(Flight::request()->ajax)
            {
                Flight::json(['error' => $error], 404);
            }
            else
            {
                Flight::render('app/error', ['error' => $error]);
                $this->stop(404);
            }
            
        });
        
        Flight::map('notAuthorized', function(){
            Flight::halt(403, 'No tiene autorización para realizar esta acción.');
        });
    }
    
    private function stop($code = 200)
    {
        Flight::translation()->close();
        Flight::stop($code);
    }
    
    private function loadConfig()
    {
        $configs = include_once("../config/config.php");
        foreach ($configs as $k_conf => $value)
        {
            Flight::set($k_conf, $value);
        }
        
        $i18n = Flight::get('i18n');
        if($i18n && count($i18n)>0)
        {
            $default = array_keys($i18n)[0];
            Flight::set('lang', $default);
        }
        
        Flight::set('urlLangRoot', Flight::get('urlRoot'));
        
        Flight::register("translation", "Translation", [$i18n]);
        Flight::map('t', [Flight::translation(), 'get'] );
        
        
        $static_config = [];
        $file_names = array('routes', 'params');
        foreach ($file_names as $name)
        {
            $static_config[$name] = include_once("../config/{$name}.php");
        }
        Flight::set('static_config', $static_config);
        
        Flight::map('params', function($key = ""){
            $static_config = Flight::get('static_config');
            if(isset($static_config['params']) && ($params = $static_config['params']))
            {
                if($key)
                {
                    if(array_key_exists($key, $params)) return $params[$key];
                    else return NULL;
                }
                else
                {
                    return $params;
                }
            }
            else
            {
                return NULL;
            }
        });
    }
    
    private function getConfig($name = NULL)
    {
        if($name && $config = Flight::get('static_config'))
        {
            return array_key_exists($name, $config)?$config[$name]:NULL;
        }
        return Flight::get('static_config');
    }
    
    private function autoLoader()
    {
        $paths_autoload = array("controllers","helpers");
        foreach( $paths_autoload as $path)
        {
            Flight::path(__DIR__.'/'.$path);
        }
    }
    
    private function init()
    {
        //Register routes
        $this->registerRoutes();
        
        Flight::set('flight.views.path',__DIR__.'/views');
    }
    
    
    private function registerRoutes()
    {
        $routes = $this->getConfig('routes');
        foreach ($routes as $pattern => $method)
        {
            if( (count($xpl = explode(".", $method, 2)) > 0))
            {
                $class = $xpl[0];
                $func = isset($xpl[1])?$xpl[1]:'index';
                Flight::route($pattern, function() use ($class, $func)
                {
                    $class .= "Controller";
                    $name = $class . "_" . $func;
                    
                    Flight::register($class, $class, [$class, $func, func_get_args()]);
                    
                    Flight::map($name, [Flight::$class(), $func] );
                    //$instance = new $class($class, $func, func_get_args());
                    //Flight::$name();
                    call_user_func_array( [Flight::app(), $name], func_get_args());
                });
            }
        }
        
    }

}

new Bootstrap();