<html>
<head>
<?=$header_content; ?>    
</head>
<body>
    <div id="page_wrapper">
        <?php
            if(isset($header_wrapper)) echo $header_wrapper;
            if(isset($navigation_wrapper)) echo $navigation_wrapper;
            if(isset($intro_wrapper)) echo $intro_wrapper;
            if(isset($wrapper)) echo $wrapper;
            if(isset($footer_wrapper)) echo $footer_wrapper;
            if(isset($copyright_wrapper)) echo $copyright_wrapper;
        ?>
    </div>
<?=$footer_content; ?>
</body>
</html>