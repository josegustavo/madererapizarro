<?php 
if(!isset($urlLangRoot))
{
    $urlLangRoot = '';
}
?>
<html>
<head>
<?=isset($header_content)?$header_content:''; ?>    
</head>
<body>
    <div id="page_wrapper">
        <?php
            if(isset($header_wrapper)) echo $header_wrapper;
            if(isset($navigation_wrapper)) echo $navigation_wrapper;
        ?>
            <div id="intro" class="text-intro">
		<h1><?=$error['title']?></h1>
            </div>
            <div id="wrapper">
                <div id="error" class="one_half">
                        <p>
                                <strong><?=$error['code']?></strong>
                                <span><?=$error['message']?></span>
                        </p>
                </div>
                <div id="error-info" class="one_half last">
                    <p><?=$error['detail']?></p>
                    <hr class="divider-dotted">
                    <p>
                        <?=Flight::t('Ir a la')?> <a href="<?=$urlLangRoot?>"><?=Flight::t('Página de inicio')?></a>
                    </p>
                </div>
                <br class="clear">
            </div>
        <?php    
            if(isset($footer_wrapper)) echo $footer_wrapper;
            if(isset($copyright_wrapper)) echo $copyright_wrapper;
        ?>
    </div>
<?=isset($footer_content)?$footer_content:''; ?>    
</body>
</html>