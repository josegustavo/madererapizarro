<?php

$emails = Flight::params('email');
$phones = Flight::params('phones');
?>
<div id="wrapper">
    <div id="post-1547" class="page-layout">
        <div class="one_third">
            <div class="icon-box icon-big">
                <div><img src="<?=$urlRoot?>images/icons/pushpin-1.png" class="icon" alt="<?=Flight::t('¿Dónde nos encontramos?')?>"></div>
                <div class="icon-desc">
                    <h2 id="donde-nos-encontramos"><?=Flight::t('¿Dónde nos encontramos?')?></h2>
                    <div>
                        <br/>
                        <p>
                        <b><?=Flight::t('Oficina Principal')?></b>:<br/> 
                        <?=Flight::t('Av. Pachacútec 1779 - Villa María del Triunfo Lima 35 - Lima - Perú')?><br/>
                        <span>(<?=Flight::t('Referencia: A 4 cuadras del Hospital María Auxiliadora – Estación María Auxiliadora Línea 1')?>)</span>
                        </p>
                        <p>
                        <b><?=Flight::t('Oficina Sucursal')?></b>:<br/> 
                        <?=Flight::t('Av. Pachacútec 1805 cruce Jr. Miguel Grau - Villa María del Triunfo Lima 35 - Lima - Perú')?> <br/>
                        <span>(<?=Flight::t('Referencia: A 5 cuadras del Hospital María Auxiliadora – Estación María Auxiliadora Línea 1')?>)</span>
                        </p>
                        <p>
                            <b><?=Flight::t('Horario de Atención')?></b>:<br/>
                            <?=Flight::t('Lunes a Sábado de 8 a.m. - 6:30 p.m.')?>
                        </p>
                        <p>
                            <b><?=Flight::t('Teléfonos')?></b>:<br/>
                        <ul>
                            <?php foreach ($phones as $phone){ ?>
                            <li><?=$phone?></li>
                            <?php } ?>
                        </ul>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="two_third last">
            <div class="fluid-width-video-wrapper" style="padding-top: 50.961038961039%;">
                <iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3900.25068377405!2d-76.954205!3d-12.1633289!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9105b8fb21e3ece1%3A0xf58eaefb3ef574e9!2sMaderera+Pizarro!5e0!3m2!1ses!2spe!4v1435979521875" width="600" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
            <script type="text/javascript">
                jQuery(document).ready(function() { jQuery("body").fitVids({customSelector: ".fluid-gmap"}); jQuery(".fluid-gmap").css("height", ""); });
            </script>
        </div>
        <div class="clear"></div>
        <div class="one_third">
            <div class="icon-box icon-big">
            <div><img src="<?=$urlRoot?>images/icons/envelope.png" class="icon" alt="<?=Flight::t('Envíenos un mensaje')?>"></div>
            <div class="icon-desc">
            <h2 id="envienos-un-mensaje"><?=Flight::t('Envíenos un mensaje')?></h2>
            <div>
                <p>&nbsp;</p>
                <p><?=Flight::t('Llene el siguiente formulario para ponerse en contacto con nosotros')?></p>
                <p><?=Flight::t('Opcionalmente puede adjuntar un excel si desea una cotización')?></p>
                <p><b><?=Flight::t('Correo electrónico')?></b></p>
                <?php if(isset($emails)){ ?>
                <p><?=Flight::t('También puede optar enviarnos un correo electrónico a:')?></p>
                <ul>
                    <?php foreach($emails as $email){ ?>
                    <li><?=Helper::hideEmail($email)?></li>
                    <?php } ?>
                </ul>
                <?php } ?>
            </div>
            </div>
            </div>
        </div>
        <div class="two_third last">
            <div class="wpcf7" id="wpcf7-f1901-p1547-o1">
                <form action="<?=$urlRoot?>contacto" method="post" class="wpcf7-form">
                    <p>
                        <span class="wpcf7-form-control-wrap your-name">
                            <input type="text" name="name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" id="my-field" aria-required="true" placeholder="<?=Flight::t('Nombre')?> (<?=Flight::t('necesario')?>)">
                        </span> 
                    </p>
                    <p>
                        <span class="wpcf7-form-control-wrap your-email">
                            <input type="text" name="email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" placeholder="<?=Flight::t('Correo')?> (<?=Flight::t('necesario')?>)">
                        </span> 
                    </p>
                    <p>
                        <span class="wpcf7-form-control-wrap your-message">
                            <textarea name="message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" placeholder="<?=Flight::t('Escriba su mensaje')?>"></textarea>
                        </span> 
                    </p>
                    <p>
                        <span class="wpcf7-form-control-wrap your-attachment">
                            <?=Flight::t('Adjunto')?> (<?=Flight::t('opcional')?>)
                        </span>
                        <span class="wpcf7-form-control-wrap your-attachment">
                            <input type="file" name="file" class="wpcf7-form-control wpcf7-text wpcf7-file wpcf7-validates-as-required wpcf7-validates-as-file" aria-required="true" placeholder="">
                            <?=Flight::t('Permitido Excel de 2MB máx')?>
                        </span> 
                    </p>
                    <p>
                        <input type="submit" value="<?=Flight::t('Enviar')?>" class="wpcf7-form-control wpcf7-submit">
                    <div class="wpcf7-response-output wpcf7-display-none"></div>
                </form>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</div>