<div id="intro_wrapper">
    <div id="intro" class="text-intro">
        <h1><?=Flight::t('Contáctenos')?></h1>
        <p>
            <?=Flight::t('Agradecemos su vista a nuestro sitio web. Si desea comunicarse con nosotros o realizar alguna consulta lo puede realizar mediante la siguiente información.')?>
        </p>
    </div>
</div>