<?php
$langs = Flight::get("i18n");
$default_lang = Flight::get('lang');
$facebook = Flight::params('facebook');
$flickr = Flight::params('flickr');
$twitter = Flight::params('twitter');

$phones = Flight::params('phones');
?>
<div id="header_wrapper">
    <div id="header_bar_wrapper">
        <div id="header_bar">
            <div id="header_bar_inner" class="rightside">
                <div id="header_tools" class="leftside">
                    <!--<div id="search" class="leftside">
                        <form action="search" method="get">
                            <input id="search-input" type="text" name="s" value="Buscar..." onfocus="if (this.value == 'Buscar...') {this.value = '';}" onblur="if (this.value == '') {this.value = 'Search...';}" />
                            <span class="search-btn"></span>
                        </form>
                    </div>-->
                    <div id="social_icons" class="leftside">
                        <?php if(isset($twitter)){ ?>
                        <a href="" target="_blank" title="Twitter" class="tiptip">
                            <img src="<?=$urlRoot?>images/socialmedia/twitter.png" alt="Twitter" />
                        </a>
                        <?php } ?>
                        <?php if(isset($facebook)){ ?>
                        <a href="<?=$facebook?>" target="_blank" title="Facebook" class="tiptip"><img src="<?=$urlRoot?>images/socialmedia/facebook.png" alt="Facebook" /></a>
                        <?php } ?>
                        <?php if(isset($flickr)){ ?>
                        <a href="" target="_blank" title="Flickr" class="tiptip"><img src="<?=$urlRoot?>images/socialmedia/flickr.png" alt="Flickr" /></a>
                        <?php } ?>
                    </div>
                    <div id="langs_icons" class="leftside">
                        <?php if(is_array($langs) && count($langs>1)) foreach ($langs as $k_lang => $lang) { ?>
                        <a <?php if($default_lang!=$k_lang){ ?>href="<?=$urlRoot.$k_lang.'/'?>" title="<?=Flight::t('Cambiar idioma a')?> <?=Flight::t($lang)?>"<?php } ?>  class="language-selection tiptip <?=($k_lang==$default_lang)?'active':'';?>">
                            <img src="<?=$urlRoot?>images/langs/<?=$k_lang?>.png" alt="<?=$lang?>" width="34" height="34" />
                        </a>
                        <?php } ?>
                    </div>
                </div>
                <?php if(count($phones)){ ?>
                <div id="header_info" class="leftside">
                    Llámanos: <strong><?=$phones[0]?></strong>				
                </div>
                <?php } ?>
                <br class="clear" />
            </div>
        </div>
    </div>
    <div id="header">
        <div id="logo"><a href="<?=$urlRoot?>"><img src="<?=$urlRoot?>images/<?=Flight::t('logo.png')?>" alt="<?=Flight::t('Maderera Pizarro')?>" /></a></div>
    </div>
</div>