<?php
$phones = Flight::params('phones');
?>
<div id="footer_wrapper">
    <div id="footer">
        <div id="text-2" class="one_fourth widgets widget_text">
            <h3><?=Flight::t('Acerca de nosotros')?></h3>
            <div class="textwidget">
                <p>
                    <img src="<?=$urlRoot?>images/<?=Flight::t('logo.png')?>" alt="<?=Flight::t('Maderera Pizarro')?>">
                </p>
                <p><?=Flight::t('Maderera Pizarro es un grupo empresarial con más de 30 años de experiencia en el rubro maderero realizando actividades de transformación y distribución de productos de maderas finas en lima y provincias del Perú.')?></p>
                <p>
                    <?php if($productos = Flight::params('productos')){ ?>
                        <?=Flight::t('Ofrecemos diversos productos como madera')?> 
                        <?php foreach ($productos as $k_prod => $producto){ ?>
                        <a href="<?=$urlLangRoot?><?=Flight::t('productos')?>/<?=Flight::t($k_prod)?>"><?=Flight::t($producto['name'])?></a>, 
                        <?php } ?>
                    <?php } ?> 
                    <?php if($servicios = Flight::params('servicios')){ ?>
                        <?=Flight::t('y diversos servicios como')?> 
                        <?php foreach ($servicios as $k_serv => $servicio){ ?>
                            <a href="<?=$urlLangRoot?><?=Flight::t('servicios')?>#<?=Flight::t($k_serv)?>" title="<?=Flight::t($servicio['name'])?>"><?=Flight::t($servicio['shortname'])?></a>, 
                        <?php } ?>
                    <?php } ?>.
                </p>
                <p><a href="<?=$urlLangRoot?><?=Flight::t('nosotros')?>"><?=Flight::t('Ver más...')?></a></p>
            </div>
        </div>
        <?php if($servicios = Flight::params('servicios')){ ?>
        <div class="one_fourth widgets">
            <h3><?=Flight::t('Servicios')?></h3>
            <ul class="recent-posts">
                <?php foreach($servicios as $k_serv => $servicio) { ?>
                <li>
                    <div class="alignleft">
                        <a href="<?=$urlLangRoot?><?=Flight::t('servicios')?>#<?=Flight::t($k_serv)?>" class="recent-link" title="<?=Flight::t($servicio['name'])?>">
                            <img src="<?=$urlRoot?>images/<?=$servicio['image']?>" alt="<?=Flight::t($servicio['name'])?>" width="50" height="50">
                        </a>
                    </div>
                    <div class="posts-desc">
                        <h4>
                            <a href="<?=$urlLangRoot?><?=Flight::t('servicios')?>#<?=Flight::t($k_serv)?>" title="<?=Flight::t($servicio['name'])?>"><?=Flight::t($servicio['shortname'])?></a>
                        </h4>
                        <p><?=Flight::t($servicio['description'])?></p>
                    </div>
                </li>
                <?php } ?>
            </ul>
        </div>
        <?php } ?>
        <?php if($productos = Flight::params('productos')){ ?>
        <div class="one_fourth widgets widget_flickr">
            <h3><?=Flight::t('Maderas')?></h3>
            <ul class="flickr">
                <?php foreach ($productos as $k_prod => $producto){ ?>
                <li>
                    <a href="<?=$urlLangRoot?><?=Flight::t('productos')?>/<?=Flight::t($k_prod)?>" title="<?=Flight::t($producto['name'])?>"><img src="<?=$urlRoot?>images/<?=$producto['texture']?>" alt="<?=Flight::t($producto['name'])?>"></a>
                </li>
                <?php } ?>
            </ul>
            <br class="clear" />
        </div>
        <?php } ?>
        <div id="recent-comments-3" class="one_fourth widgets widget_recent_comments">
            <h3><?=Flight::t('Ubíquenos')?></h3>
            <div>
                <p>
                <b><?=Flight::t('Oficina Principal')?></b>:<br> 
                <?=Flight::t('Av. Pachacútec 1779 - Villa María del Triunfo Lima 35 - Lima - Perú')?> <br>
                <span>(<?=Flight::t('Referencia: A 4 cuadras del Hospital María Auxiliadora – Estación María Auxiliadora Línea 1')?>)</span>
                </p>
                <p>
                    <b><?=Flight::t('Horario de Atención')?></b>:<br>
                    <?=Flight::t('Lunes a Sábado de 8 a.m. - 6:30 p.m.')?>
                </p>
                <p>
                    <b><?=Flight::t('Teléfonos')?></b>:<br>
                </p><ul>
                    <?php foreach ($phones as $phone){ ?>
                    <li><?=$phone?></li>
                    <?php } ?>
                </ul> 
                <p></p>
            </div>
        </div>
        <br class="clear" />
    </div>
</div>