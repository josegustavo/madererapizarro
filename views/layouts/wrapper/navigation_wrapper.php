<div id="navigation_wrapper">
    <div id="navigation">
    	<ul id="menu-top-menu" class="sf-menu">
            <?php foreach ($menus as $menu) { ?>
            <li class="menu-item menu-item-type-custom menu-item-object-custom <?php (isset($menu['active']) && $menu['active'] == true)?'current-menu-item':'';?>">
                <a href="<?=$urlLangRoot?><?php if(isset($menu['url']))echo $menu['url'];?>"><span class="menu-btn"><?=$menu['name'];?></span></a>
                <?php if(isset($menu['submenu'])){ ?>
                <ul class="sub-menu">
                    <?php  $submenus = $menu['submenu'];foreach ($submenus as $submenu){ ?>
                    <li class="menu-item menu-item-type-post_type menu-item-object-page <?php (isset($menu['active']) && $menu['active'] == true)?'current-menu-item':'';?>">
                        <a href="<?=$urlLangRoot?><?php if(isset($menu['url'])) echo $menu['url']; if(isset($submenu['url'])) echo $submenu['url'];?>"><span class="menu-btn"><?=$submenu['name'];?></span></a>
                    </li>
                    <?php } ?>
                </ul>
                <?php } ?>    
            </li>
            <?php } ?>
        </ul>
        <br class="clear" />
    </div>
</div>