<div id="wrapper">
    <?php if($productos = Flight::params('productos')){ ?>
    <script type="text/javascript">
        jQuery(window).load(function() {
            jQuery("#flexslider_1").krioImageLoader({ onStart: function() {
            jQuery(this).css("min-height", 0).css("background", "none");
            jQuery(this).flexslider({prevText: "Previous", nextText: "Next"});
            } });
        });
    </script>
    <script type="text/javascript">jQuery(window).load(function() { jQuery("#scrollable_1").show(); jQuery(".list-carousel").css("min-height", 0).css("background", "none"); jQuery("#scrollable_1").carouFredSel({next: "#snext_1", prev: "#sprev_1", responsive: true, debug: false,				items: { visible: { min: 1, max: 6 }, width: 170 },				scroll: { wipe: true, items: 1, fx: "scroll", easing: "swing", duration: 500, pauseOnHover: false },				auto: { play: false, pauseDuration: 2500 },				infinite: false,				circular: false,pagination: { container: "#scrollable_1_pagination" }}); })</script>
    <div class="flexslider" id="flexslider_1">
        <ul class="slides">
            <?php foreach ($productos as $slide) { if(isset($slide['slide'])) { ?>
            <li>
                <img src="<?=$urlRoot?>images/<?=$slide['slide']?>" alt="<?=Flight::t($slide['name'])?>" />
                <p class="flex-caption"><?=Flight::t($slide['name'])?></p>
            </li>
            <?php }} ?>
        </ul>
    </div>
    <?php } ?>

<div class="fancy-box full-box">
    <h2 class="fancy-header"><span><?=Flight::t('Ofrecemos')?></span></h2>
<div class="one_third">
    <div class="icon-box icon-big">
        <div>
            <img src="<?=$urlRoot?>images/icons/cortes.png" class="icon" alt="<?=Flight::t('Cortes')?>" />
        </div>
        <div class="icon-desc">
            <h2><?=Flight::t('Cortes especiales')?></h2>
            <div>
                <span><?=Flight::t('Ofrecemos una variedad de cortes  de acuerdo a las necesidades del cliente.')?></span>
            </div>
        </div>
    </div>
</div>
<div class="one_third">
    <div class="icon-box icon-big">
        <div>
            <img src="<?=$urlRoot?>images/icons/time.png" class="icon" alt="<?=Flight::t('Despacho a tiempo')?>" />
        </div>
        <div class="icon-desc">
            <h2><?=Flight::t('Despacho a tiempo')?></h2>
            <div>
                <span><?=Flight::t('Nuestra eficiencia y experiencia nos permiten realizar despachos a tiempo.')?></span>
            </div>
        </div>
    </div>
</div>

<div class="one_third last">
    <div class="icon-box icon-big">
        <div>
            <img src="<?=$urlRoot?>images/icons/buoy.png" class="icon" alt="<?=Flight::t('Atención personalizada')?>" />
        </div>
        <div class="icon-desc">
            <h2><?=Flight::t('Atención personalizada')?></h2>
            <div>
                <span><?=Flight::t('Atención rápida y confiable nos diferencian, debido a que resolvemos tus necesidades.')?></span>
            </div>
        </div>
    </div>
</div>
<div class="clear"></div>
</div>

<?php if($servicios = Flight::params('servicios')){ ?>
<h2 class="fancy-header"><span><?=Flight::t('Servicios')?></span></h2>
<div class="fancy_list_wrapper">
    <?php $i=0; foreach($servicios as $k_serv => $servicio){ if(++$i>3)break;?>
    <div class="one_third fancy_list_item <?=($i==3)?'last':''?>">
        <div class="fancy_image">
            <a href="<?=$urlLangRoot?><?=Flight::t('servicios')?>#<?=Flight::t($k_serv)?>">
                <img src="<?=$urlRoot?>images/<?=$servicio['image']?>" class="border-img img_medium img-height-200" width="370" alt="<?=Flight::t($servicio['shortname'])?>"/>
            </a>
            <div class="fancy_meta">
                <ul>
                    <li>
                        <a href="<?=$urlLangRoot?><?=Flight::t('servicios')?>#<?=Flight::t($k_serv)?>" class="tiptip fancy_icon fancy_details" title="<?=Flight::t('Más detalles')?>"><span></span></a>
                    </li>
                </ul>
            </div>
        </div>
        <h3><a href="<?=$urlRoot?><?=Flight::t('servicios')?>#<?=Flight::t($k_serv)?>"><?=Flight::t($servicio['shortname'])?></a></h3>
        <div class="fancy_categories">
            <?=Flight::t($servicio['description'])?>
        </div>
    </div>
    <?php } ?>
    <div class="clear"></div>
</div>
<?php } ?>

<hr class="divider-full" />
<?php if($productos = Flight::params('productos')){ ?>
<div class="one_half">
    <h2 class="fancy-header"><span><?=Flight::t('Maderas')?></span></h2>
    <ul class="tabs">
        <?php $i = 0; foreach ($productos as $k_madera => $madera){ ?>
        <li class="<?=($i==0)?'current':''?>">
            <a href="#" class="tab">
                <img src="<?=$urlRoot?>images/<?=$madera['icon']?>" width="25" alt="<?=$madera['name']?>" />
            </a>
        </li>
        <?php $i++; } ?>
    </ul>
    <?php foreach ($productos as $k_madera => $madera){ ?>
    <div class="tab_content fancy-box">
        <h3><?=$madera['name']?></h3>
        <p><?=$madera['description']?></p>
        <p><a href="<?=$urlLangRoot?><?=Flight::t('productos')?>/<?=Flight::t($k_madera)?>"><?=Flight::t('Ver más...')?></a></p>
    </div>
    <?php } ?>
</div>
<?php }?>

<?php if(($news = Helper::getNews()) && count($news)){ ?>
<div class="one_half last">
    <h2 class="fancy-header"><span><?=Flight::t('Noticias')?></span></h2>
    <div class="widgets">
        <ul class="recent-posts">
            <?php foreach ($news as $new) { ?>
            <li class="group">
                <div class="meta-date">
                    <span class="meta-day"><?=$new['day']?></span>
                    <span class="meta-month"><?=$new['month']?></span>
                </div>
                <div class="posts-desc">
                    <h4><?=$new['title']?></h4>
                    <div class="fancy_categories">
                        <?=$new['subtitle']?>
                    </div>
                    <p><?=$new['content']?></p>
                </div>
            </li>
            <?php } ?>
        </ul>
    </div>
</div>
<?php } ?>

<div class="clear"></div>


</div>