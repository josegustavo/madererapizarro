<div id="copyright_wrapper">
    <div id="copyright" class="group">
    	<div id="footer_nav" class="full_page">
            <ul id="menu-bottom-menu" class="">
                <?php foreach ($menus as $menu) { ?>
                <li class="menu-item menu-item-type-post_type menu-item-object-page">
                    <a href="<?=$urlRoot?><?=$menu['url']?>"><?=$menu['name']?></a>
                </li>
                <?php } ?>
            </ul>
        </div>
    	<hr />
    	<div class="one_half"><?=Flight::t('Copyright © 2015')?> <a href="<?=$urlRoot?>"><strong><?=Flight::t('Maderera Pizarro')?></strong></a>. <?=Flight::t('Todos los derechos reservados.')?></div>
        <!--<div class="one_half last right">
            <?=Flight::t('Powered by:')?> <a href="http://fb.com/653303905" target="_blank"><strong><?=Flight::t('JG')?></strong></a>
        </div>-->
    </div>
</div>