<?php
$content = array_filter(Flight::translation()->getContent());
?>
<div id="wrapper">
    <div class="page-layout">
        <div class="full_page">
            <form method="post">
            <div class="wpcf7 ">
                <?php $i=0; foreach ($content as $k => $v){ $i++ ?>
                    <p class="message <?=($k==$v)?'msg-error':'msg-info'?>">
                        <?=$k?>
                        <br/>
                        <input type="hidden" name="item<?=$i?>[key]" value="<?=$k?>" />
                        <input type="text" name="item<?=$i?>[value]" value="<?=$v?>"/>
                    </p>
                <?php } ?>
                <p>
                    <input type="submit" value="Guardar" class="wpcf7-form-control wpcf7-submit">
                </p>
            </div>
            </form>
        </div>
        <div class="clear"></div>
    </div>
    
</div>