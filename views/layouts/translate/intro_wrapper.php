<?php
$langs = Flight::get("i18n");
$default_lang = Flight::get('lang');
$selected = isset($langs[$default_lang])?$langs[$default_lang]:NULL;
?>
<div id="intro_wrapper">
    <div id="intro" class="text-intro">
        <h1>Traducir el sitio</h1>
        <?php if($selected){ ?>
        <p><?=$selected?></p>
        <?php } ?>
    </div>
</div>