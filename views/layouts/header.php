<meta charset="utf-8" />
<title><?=Flight::t('Maderera Pizarro')?> <?=isset($subtitle)?'| '.Flight::t($subtitle):''?></title>
<meta name="description" content="<?=Flight::t('Página web de la empresa Maderera Pizarro SAC')?>" />
<meta name="keywords" content="<?=Flight::t('Maderera, pizarro, carpintería, maderas, arboles, cedros')?>" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

<link rel="stylesheet" type="text/css" media="all" href="<?=$urlRoot?>css/style.css" />
<link rel="stylesheet" type="text/css" media="all" href="<?=$urlRoot?>css/mediaqueries.css" />
<link rel='stylesheet' type="text/css" href='<?=$urlRoot?>css/demo-sidebar.css' type='text/css' media='all' />
<link rel='stylesheet' type="text/css" href='<?=$urlRoot?>css/sliders.css' type='text/css' media='all' />
<link rel='stylesheet' type="text/css" href='<?=$urlRoot?>css/galleria.classic.css' type='text/css' media='all' />
<link rel='stylesheet' type="text/css" href='<?=$urlRoot?>css/menu.css' type='text/css' media='all' />
<link rel='stylesheet' type="text/css" href='<?=$urlRoot?>css/tipsy.css' type='text/css' media='all' />
<link rel='stylesheet' type="text/css" href='<?=$urlRoot?>css/prettyphoto.css' type='text/css' media='all' />
<link rel='stylesheet' type="text/css" href='http://fonts.googleapis.com/css?family=Droid+Sans' type='text/css' media='all' />
<link rel='stylesheet' type="text/css" href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow' type='text/css' media='all' />
<link rel='stylesheet' type="text/css" href='http://fonts.googleapis.com/css?family=PT+Sans' type='text/css' media='all' />
<link rel='stylesheet' type="text/css" href='<?=$urlRoot?>css/styles.css' type='text/css' media='all' />

<script type='text/javascript' src='<?=$urlRoot?>js/jquery.js'></script>
<script type='text/javascript' src='<?=$urlRoot?>js/jquery-migrate.min.js'></script>
<script type='text/javascript' src='<?=$urlRoot?>js/demo-sidebar.js'></script>
<!--<script type='text/javascript' src='<?=$urlRoot?>js/colorpicker.js'></script>-->
<script type='text/javascript' src='<?=$urlRoot?>js/preloader.js'></script>
<script type='text/javascript' src='<?=$urlRoot?>js/respond.js'></script>
<script type='text/javascript' src='<?=$urlRoot?>js/hoverIntent.js'></script>
<script type='text/javascript' src='<?=$urlRoot?>js/superfish.js'></script>
<script type='text/javascript' src='<?=$urlRoot?>js/jquery.prettyphoto.js'></script>
<script type='text/javascript' src='<?=$urlRoot?>js/comment-reply.min.js'></script>

<link href="<?=$urlRoot?>css/style_01.css" rel="stylesheet" type="text/css" />

<style type="text/css">body { font-family: 'Droid Sans', arial, serif; }
h1,.flex-caption,.ei-title h2 { font-family: 'PT Sans Narrow', arial, serif; }
h2,.ei-title h3 { font-family: 'PT Sans Narrow', arial, serif; }
h3,.rscaption { font-family: 'PT Sans Narrow', arial, serif; }
h4 { font-family: 'PT Sans Narrow', arial, serif; }
h5 { font-family: 'PT Sans Narrow', arial, serif; }
h6 { font-family: 'PT Sans Narrow', arial, serif; }
.sf-menu li { font-family: 'PT Sans Narrow', arial, serif; }
.sf-menu li li { font-family: 'PT Sans', arial, serif; }
</style>	
<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
<script>
    window.urlRoot = "<?=$urlRoot?>";
</script>