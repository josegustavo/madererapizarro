<div id="wrapper">
    <div class="page-layout">
        <div class="full_page">
            <p class="text-justify">
                <?=Flight::t('Maderera Pizarro busca hacer un uso sostenible de la materia prima como es la madera, evitando y reduciendo los impactos negativos de nuestras operaciones y asegurar su capacidad productiva a futuro.')?>
            </p>
            <p class="text-justify">
                <?=Flight::t('Siendo la forestería la actividad que menos impacto genera en el suelo amazónico, si se realiza adecuadamente permitiría explotar un recurso renovable sin comprometer las necesidades de futuras generaciones, y así generar permanente riqueza en las zonas de la región.')?>
            </p>
            <p class="text-justify">
                <?=Flight::t('Al realizar las diversas actividades se pretende asegurar el cumplimiento de los derechos de nuestros colaboradores y ser un miembro activo en el desarrollo de los mismos. Por otro lado, ofrecer soluciones de calidad a nuestros clientes y promover el manejo sostenible de los bosques a nivel nacional e internacional procurando la continuidad de la empresa y sostenibilidad en el tiempo.')?>
            </p>
            <p class="text-justify">
                <?=Flight::t('Adicionalmente, se debe considerar que menos del 30% de los bosques de producción permanente están puestos en valor. Esto demuestra que la forestería aún está muy por debajo de su potencial, de acuerdo a las cuales la actividad representa menos del 1% de la economía en el Perú.')?>
            </p>
            <p class="center">
                <img src="<?=$urlRoot?>images/sostenibilidad.jpg" class="border-img img_large"/>
            </p>
            <div class="clear"></div>
        </div>
    </div>
</div>