<div id="wrapper" class="sidebar-right-wrapper">
    <div id="page-content" class="two_third">
        <div class="page-layout">
            <h2 class="fancy-header"><span><?=Flight::t('Maderas')?></span></h2>
            <div class="full_page">
                <p>
                    <?=Flight::t('En Maderera Pizarro contamos con más de 30 años de experiencia en la transformación de la madera y sus actividades relacionadas como el aserrío y cepillado de la misma.')?>
                </p>
                <p>
                    <?=Flight::t('Además, se ofrece una variedad de especies de madera aserrada, cepillada y machihembrada, para atender y satisfacer las distintas necesidades de nuestros clientes poniendo gran atención en la calidad del producto en las etapas de su procesamiento.')?>
                </p>
                <p class="center">
                    <img src="<?=$urlRoot?>images/maderas1.jpg" class="border-img img_medium"/>
                </p>
                <p>
                    <?=Flight::t('Ofrecemos los siguientes tipos de madera:')?>
                </p>
                <p>
                    <ul class="small-list small-arrow-dark">
                        <li class="page_item"><a href="">Capirona</a></li>
                        <li class="page_item"><a href="">Cashimbo</a></li>
                        <li class="page_item"><a href="">Catahua</a></li>
                        <li class="page_item"><a href="">Copayba</a></li>
                        <li class="page_item"><a href="">Huayruro</a></li>
                        <li class="page_item"><a href="">Roble</a></li>
                        <li class="page_item"><a href="">Tornillo</a></li>
                    </ul>
                </p>
            </div>
            <div class="clear"></div>
            <hr class="divider-dotted">
            <h2 class="fancy-header"><span><?=Flight::t('Especificación Técnica')?></span></h2>
            <div class="full_page">
                <p><?=Flight::t('Las maderas cuentan con una especificación de acuerdo a las dimensiones que poseen. Las dimensiones básicas son tres: largo, ancho y espesor en unidades de longitud.')?></p>
                <p><?=Flight::t('Es importante conocer y establecer las dimensiones de las maderas para la solicitud y compra de las mismas, respectivamente.')?></p>
                <p class="center">
                    <img src="<?=$urlRoot?>images/maderas2.jpg" class="border-img img_medium"/>
                </p>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="one_third last sidebar-right">
        <div class="widgets widget_subpages">
            <h2 class="fancy-header"><span><?=Flight::t('Tipos de maderas')?></span></h2>
            <ul>
                <li class="page_item"><a href="<?=$urlLangRoot?><?=Flight::t('productos')?>/capirona">Capirona</a></li>
                <li class="page_item"><a href="<?=$urlLangRoot?><?=Flight::t('productos')?>/cashimbo">Cashimbo</a></li>
                <li class="page_item"><a href="<?=$urlLangRoot?><?=Flight::t('productos')?>/catahua">Catahua</a></li>
                <li class="page_item"><a href="<?=$urlLangRoot?><?=Flight::t('productos')?>/copayba">Copayba</a></li>
                <li class="page_item"><a href="<?=$urlLangRoot?><?=Flight::t('productos')?>/huayruro">Huayruro</a></li>
                <li class="page_item"><a href="<?=$urlLangRoot?><?=Flight::t('productos')?>/roble">Roble</a></li>
                <li class="page_item"><a href="<?=$urlLangRoot?><?=Flight::t('productos')?>/tornillo">Tornillo</a></li>
            </ul>
        </div>
    </div>
</div>