<?php
$maderas = Flight::params('productos');
$madera = $maderas['cashimbo'];
?>
<div id="wrapper" class="sidebar-right-wrapper">
    <div id="page-content" class="two_third">
        <div class="page-layout">
            <h2 class="fancy-header"><span><?=Flight::t('Descripción')?></span></h2>
            <div class="full_page">
                <?php foreach ($madera['details'] as $title => $detail){ ?>
                <h3><?=Flight::t($title)?></h3>
                <div class="table-border">
                    <table class="tables table-light highlight-row">
                        <tbody>
                            <?php foreach ($detail as $th => $td){ ?>
                            <tr>
                                <th class="left"><?=Flight::t($th)?></th>
                                <td><?=Flight::t($td)?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <?php } ?>
            </div>
            <div class="clear"></div>
            <p>
                <a href="<?=$urlLangRoot?><?=Flight::t('contacto')?>#<?=Flight::t('envienos-un-mensaje')?>"><?=Flight::t('Solicitar más información')?></a>
            </p>
        </div>
    </div>
    <div class="one_third last sidebar-right">
        <div class="widgets widget_subpages">
            <?php if(isset($madera['image']) && $madera['image']){ ?>
            <h2 class="fancy-header"><span><?=Flight::t('Imagen')?></span></h2>
            <div class="full-box">
                <img src="<?=$urlRoot?>images/<?=$madera['image']?>" alt="<?=Flight::t('Imagen')." ".$madera['name']?>" class="border-img img_medium">
            </div>
            <?php } ?>
            <?php if(isset($madera['texture']) && $madera['texture']){ ?>
            <h2 class="fancy-header"><span><?=Flight::t('Textura')?></span></h2>
            <div class="full-box">
                <img src="<?=$urlRoot?>images/<?=$madera['texture']?>" alt="<?=Flight::t('Textura')." ".$madera['name']?>" class="border-img img_medium">
            </div>
            <?php } ?>
            <h2 class="fancy-header"><span><?=Flight::t('Otros tipos de maderas')?></span></h2>
            <ul>
                <?php foreach ($maderas as $k_madera => $madera){ ?>
                <li class="page_item">
                    <a href="<?=$urlLangRoot?><?=Flight::t('productos')?>/<?=$k_madera?>"><?=$madera['name']?></a>
                </li>
                <?php } ?>
            </ul>
        </div>
    </div>
</div>