<div id="wrapper">
    <div class="page-layout">
        <h2 id="corte" class="fancy-header"><span><?=Flight::t('Corte o Tableado de Madera')?></span></h2>
        <div class="full_page">
            <p>
                <?=Flight::t('Maderera Pizarro ofrece diversos servicios para el tratado de la madera y/o producto. El corte de la madera consiste en la transformación de una troza de madera a un producto con dimensiones específicas de ancho, largo y espesor, con el fin de ser utilizado en un proceso posterior, como lo es la fabricación de muebles, casas, entre otros. Al realizarse el corte de las trozas se producen adicionalmente polvos o pequeños granos de madera (aserrín).')?>
            </p>
            <p class="center">
                <img src="<?=$urlRoot?>images/servicios1.jpg" class="border-img img_medium"/>
            </p>
        </div>
        <div class="clear"></div>
        
        <h2 id="cepillado" class="fancy-header"><span><?=Flight::t('Cepillado de Madera')?></span></h2>
        <div class="full_page">
            <p>
                <?=Flight::t('Además del tableado de maderas, Maderera Pizarro ofrece el servicio de cepillado. El cepillado de la madera consiste en quitar las irregularidades del producto y emparejar la superficie de la madera. Diversas son las herramientas que se emplean y si bien todas cumplen la misma función, es decir, sacar laminillas de madera (viruta), no pueden utilizarse indistintamente.')?>
            </p>
            <div class="one_half">
                <p class="center">
                    <img src="<?=$urlRoot?>images/servicios2_1.jpg" class="border-img img_medium"/>
                </p>
            </div>
            <div class="one_half last">
                <p class="center">
                    <img src="<?=$urlRoot?>images/servicios2_2.jpg" class="border-img img_medium"/>
                </p>
            </div>
        </div>
        <div class="clear"></div>
        
        <h2 id="machihembrado" class="fancy-header"><span><?=Flight::t('Machihembrado de Madera')?></span></h2>
        <div class="one_third">
            <div class="shadow shadow_medium"><a href=""><img src="<?=$urlRoot?>images/servicios3_1.jpg" alt="" class="border-img img_medium"></a></div>
            <h2></h2>
            <p><?=Flight::t('El machihembrado es un servicio de ensamble de tablas de madera cepillada por medio de rebajes y cortes en sus cantos, para lograr por medio de la sucesión de piezas encajadas entre sí una sola superficie lisa, uniforme y sólida.')?></p>
        </div>
        <div class="one_third">
            <div class="shadow shadow_medium"><a href=""><img src="<?=$urlRoot?>images/servicios3_2.jpg" alt="" class="border-img img_medium"></a></div>
            <h2></h2>
            <p><?=Flight::t('De este modo, se labra en los cantos de la tabla dos tipos de perfilado: macho, en forma de pestaña sobresaliente, y hembra, en forma de canal; sus medidas están pensadas para lograr una unión perfecta. Para ensamblar las tablas, se encaja el canto cortado en macho de una pieza dentro del canto cortado en hembra de otra pieza, quedando unidas para soportar las cargas propias del uso.')?></p>
        </div>
        <div class="one_third last">
            <div class="shadow shadow_medium"><a href=""><img src="<?=$urlRoot?>images/servicios3_3.jpg" alt="" class="border-img img_medium"></a></div>
            <h2></h2>
            <p><?=Flight::t('Este sistema es utilizado principalmente para pisos de madera, donde se busca lograr una superficie lisa e indeformable frente a la aplicación de las cargas del uso. Para tablas largas (de 3,20 m de longitud) se labran sólo dos de los cuatro cantos, pues el dimensionamiento en obra de las piezas no justifica labrar los cuatro. Donde sí se da el uso de machihembrado en los cuatro cantos es en palmetas para parqué, entre otros.')?></p>
        </div>
        
        <div class="clear"></div>
        
        
        <h2 id="transporte" class="fancy-header"><span><?=Flight::t('Transporte de Carga')?></span></h2>
        <div class="full_page">
            <p>
                <?=Flight::t('Maderera Pizarro ofrece el servicio de transporte de madera o carga. Puede consultar precios y cobertura de este servicio en los medios de contacto de la maderera.')?>
            </p>
            <p class="center">
                <img src="<?=$urlRoot?>images/servicios4.jpg" class="border-img img_medium"/>
            </p>
        </div>
        <div class="clear"></div>
    </div>
</div>