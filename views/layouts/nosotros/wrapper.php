<div id="wrapper">
    <div class="page-layout">
        <h2 id="<?=Flight::t('quienes-somos')?>"><?=Flight::t('¿Quiénes somos?')?></h2>
        <div class="full_page">
            <p>
                <?=Flight::t('Maderera Pizarro es un grupo empresarial con más de 30 años de experiencia en el rubro maderero realizando actividades de transformación y distribución de productos de maderas finas en lima y provincias del Perú.')?>
            </p>
            <p>
                <?=Flight::t('En el sector maderero, somos proveedores y comercializadores, contamos con una amplia variedad de especies de madera y ofrecemos diversos servicios de aserrío, cepillado, machihembrado, entre otros, de modo que se pueda atender y satisfacer las distintas necesidades de nuestros clientes poniendo gran atención en la calidad del producto en sus diferentes etapas.')?>
            </p>
            <p>
                <?=Flight::t('Desde nuestros inicios nos hemos caracterizado por mantener altos niveles de calidad en nuestros productos y servicios. Por ello, hemos generado relaciones de largo plazo con nuestros clientes y proveedores, convirtiéndonos en una de las principales organizaciones comercializadoras de productos de madera en el Perú.')?>
            </p>
            <p class="center">
                <img src="<?=$urlRoot?>images/nosotros1.jpg" class="border-img img_medium"/>
            </p>
        </div>
        <div class="clear"></div>
        <hr class="divider-dotted">
        <h2 id="<?=Flight::t('mision-y-vision')?>"><?=Flight::t('Misión y Visión')?></h2>
        <div class="full_page">
            <p>
                <b><?=Flight::t('Misión')?></b>:
                <br/> 
                <?=Flight::t('Satisfacer las necesidades más exigentes de nuestros clientes, brindándoles plena excelencia en los productos y servicios desde la transformación, comercialización local, nacional; de todo tipo de maderas a precios sumamente competitivos, teniendo como principio básico: Productos y servicios de excelencia con un estricto control de calidad.')?>
            </p>
            <p>
                <b><?=Flight::t('Visión')?></b>:
                <br/> 
                <?=Flight::t('Llegar a ser el principal grupo maderero del Perú ubicándonos a la vanguardia de las más grandes exigencias del rubro maderero; diversificando nuestros productos y servicios a nuestros clientes y proveedores así como manteniendo principios de conservación ambiental de los recursos naturales.')?>
            </p>
            <p class="center">
                <img src="<?=$urlRoot?>images/nosotros2.jpg" class="border-img img_medium"/>
            </p>
        </div>
        <div class="clear"></div>
    </div>
</div>