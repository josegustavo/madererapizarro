<?php

class Helper
{
    public static function getMenus($current = "")
    {
        $menus = Flight::params('menus');
        foreach ($menus as $k => $menu)
        {
            if(isset($menu['name']))
                $menus[$k]['name'] = Flight::t($menu['name']);
            if(isset($menu['url']))
                $menus[$k]['url'] = Flight::t($menu['url']);
            if(isset($menu['submenu']) && ($submenus = $menu['submenu']))
                foreach ($submenus as $k_submenu => $submenu)
                {
                    if(isset($submenu['name']))
                        $menus[$k]['submenu'][$k_submenu]['name'] = Flight::t($submenu['name']);
                    if(isset($submenu['url']))
                        $menus[$k]['submenu'][$k_submenu]['url'] = Flight::t($submenu['url']);
                }
        }
        return $menus;
    }

    public static function getNews()
    {
        $news = Flight::params('news');
        foreach ($news as $k => $new)
        {
            if(isset($new['month'])) $new[$k]['month'] = Flight::t($new['month']);
            if(isset($new['title'])) $new[$k]['title'] = Flight::t($new['title']);
            if(isset($new['subtitle'])) $new[$k]['subtitle'] = Flight::t($new['subtitle']);
            if(isset($new['content'])) $new[$k]['content'] = Flight::t($new['content']);
        }
        return $news;
    }

    static function reverseTranslate(&$args)
    {
        $translation = Flight::translation();
        foreach ($args as $k => $val)
        {
            $args[$k] = $translation->getReverse($val);
        }
    }
    
    static function hideEmail($email, $innerHTML = NULL)
    {
        if(!$email) return;
        $character_set = '+-.0123456789@ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz';
        $key = str_shuffle($character_set); $cipher_text = ''; $id = 'e'.rand(1,999999999);
        for ($i=0;$i<strlen($email);$i+=1) $cipher_text.= $key[strpos($character_set,$email[$i])];
        $script = 'var a="'.$key.'";var b=a.split("").sort().join("");var c="'.$cipher_text.'";var d="";';
        $script.= 'for(var e=0;e<c.length;e++)d+=b.charAt(a.indexOf(c.charAt(e)));';
        $script.= 'document.getElementById("'.$id.'").innerHTML="<a href=\\"mailto:"+d+"\\">"+'. ($innerHTML?"'$innerHTML'":'d') . '+"</a>"';
        $script = "eval(\"".str_replace(array("\\",'"'),array("\\\\",'\"'), $script)."\")"; 
        $script = '<script type="text/javascript">/*<![CDATA[*/'.$script.'/*]]>*/</script>';
        return '<a id="'.$id.'"></a>'.$script;
    }
    
    static function cleanInput($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
     }
}