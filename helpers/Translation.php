<?php

class Translation
{
    
    private $i18nPath       = '';
    private $dirty          = FALSE;
    
    private $content        = NULL;

    private $langs          = [];
    private $lang_default   = NULL;
    
    public function __construct(Array $langs)
    {
        $this->i18nPath = __DIR__.'/../i18n/';
        $this->langs = $langs;
        
        if(count($langs))
        {
            $this->lang_default = array_keys($langs)[0];
        }
        
        register_shutdown_function([&$this, 'close']);
    }
    

    public function setLang($lang_default = NULL)
    {
        $this->lang_default = strtolower($lang_default);
        
        if($lang_default && !array_key_exists($lang_default, $this->langs) && (count($this->langs)))
        {
            $this->lang_default = array_keys($this->langs)[0];
        }
        $this->close();
    }
    
    public function getLangDefault()
    {
        return $this->lang_default;
    }


    public function open()
    {
        if ($this->content !== NULL) {
            return;
        }
        
        $path = $this->i18nPath . $this->lang_default . ".json";
        if(file_exists($path) && ($content = file_get_contents($path)))
        {
            $this->content = json_decode($content, true);
        }
        
        if($this->content == NULL)
        {
            $this->content = [];
        }
        
        //$error = error_get_last();
        //$message = isset($error['message']) ? $error['message'] : 'Failed to start session.';
        //throw new Exception(__METHOD__ . ":" . $message);
    }


    public function close()
    {
        if ($this->dirty)
        {
            $path = $this->i18nPath . $this->lang_default . ".json";
            $content = json_encode($this->content);
            file_put_contents($path, $content);
            $this->content = NULL;
            $this->dirty = FALSE;
        }
    }

    public function getContent()
    {
        $this->open();
        return $this->content;
    }
    
    public function get($key, $defaultValue = null)
    {
        $this->open();
        if(!isset($this->content[$key])) $this->set($key, $defaultValue);
        
        return $this->content[$key];
    }

    
    public function set($key, $value)
    {
        $this->open();
        
        $this->content[$key] = ($value === NULL)?$key:$value;
        
        $this->dirty = TRUE;
    }

    public function getReverse($key)
    {
        $this->open();
        
        if(($exist = array_search($key, $this->content)) !== FALSE )
        {
            return $exist;
        }
        return $key;
    }
    
    public function remove($key)
    {
        $this->open();
        if (isset($_SESSION[$key])) {
            $value = $_SESSION[$key];
            unset($_SESSION[$key]);

            return $value;
        } else {
            return null;
        }
    }

    
    public function removeAll()
    {
        $this->open();
        foreach (array_keys($_SESSION) as $key) {
            unset($_SESSION[$key]);
        }
    }

    
    public function has($key)
    {
        $this->open();
        return isset($_SESSION[$key]);
    }

}