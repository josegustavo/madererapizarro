<?php/*
class Session
{
    
    private $_cookieParams = ['httponly' => true];

    
    public function init()
    {
        register_shutdown_function([$this, 'close']);
    }

    
    public function open()
    {
        if ($this->getIsActive()) {
            return;
        }

        $this->setCookieParamsInternal();

        @session_start();

        if ($this->getIsActive()) {
            
        } else {
            $error = error_get_last();
            $message = isset($error['message']) ? $error['message'] : 'Failed to start session.';
            throw new Exception(__METHOD__ . ":" . $message);
        }
    }


    
    public function close()
    {
        if ($this->getIsActive()) {
            @session_write_close();
        }
    }

    
    public function destroy()
    {
        if ($this->getIsActive()) {
            @session_unset();
            @session_destroy();
        }
    }

    
    public function getIsActive()
    {
        return session_status() == PHP_SESSION_ACTIVE;
    }

    private $_hasSessionId;

    
    public function getHasSessionId()
    {
        if ($this->_hasSessionId === null) {
            $name = $this->getName();
            
            if (!empty($_COOKIE[$name]) && ini_get('session.use_cookies')) {
                $this->_hasSessionId = true;
            } elseif (!ini_get('session.use_only_cookies') && ini_get('session.use_trans_sid')) {
                $this->_hasSessionId = $_GET[$name] !== null;
            } else {
                $this->_hasSessionId = false;
            }
        }

        return $this->_hasSessionId;
    }

    
    public function setHasSessionId($value)
    {
        $this->_hasSessionId = $value;
    }

    
    public function getId()
    {
        return session_id();
    }

    
    public function setId($value)
    {
        session_id($value);
    }

    
    public function regenerateID($deleteOldSession = false)
    {
        // add @ to inhibit possible warning due to race condition
        @session_regenerate_id($deleteOldSession);
    }

    
    public function getName()
    {
        return session_name();
    }

    
    public function setName($value)
    {
        session_name($value);
    }

    
    public function getSavePath()
    {
        return session_save_path();
    }

    
    public function setSavePath($value)
    {
        $path = $value;
        if (is_dir($path)) {
            session_save_path($path);
        } else {
            throw new InvalidParamException("Session save path is not a valid directory: $value");
        }
    }

    
    public function getCookieParams()
    {
        return array_merge(session_get_cookie_params(), array_change_key_case($this->_cookieParams));
    }

    
    public function setCookieParams(array $value)
    {
        $this->_cookieParams = $value;
    }

    
    private function setCookieParamsInternal()
    {
        $data = $this->getCookieParams();
        extract($data);
        if (isset($lifetime, $path, $domain, $secure, $httponly)) {
            session_set_cookie_params($lifetime, $path, $domain, $secure, $httponly);
        } else {
            throw new InvalidParamException('Please make sure cookieParams contains these elements: lifetime, path, domain, secure and httponly.');
        }
    }

    
    public function getUseCookies()
    {
        if (ini_get('session.use_cookies') === '0') {
            return false;
        } elseif (ini_get('session.use_only_cookies') === '1') {
            return true;
        } else {
            return null;
        }
    }

    
    public function setUseCookies($value)
    {
        if ($value === false) {
            ini_set('session.use_cookies', '0');
            ini_set('session.use_only_cookies', '0');
        } elseif ($value === true) {
            ini_set('session.use_cookies', '1');
            ini_set('session.use_only_cookies', '1');
        } else {
            ini_set('session.use_cookies', '1');
            ini_set('session.use_only_cookies', '0');
        }
    }

    
    public function getGCProbability()
    {
        return (float) (ini_get('session.gc_probability') / ini_get('session.gc_divisor') * 100);
    }

    
    public function setGCProbability($value)
    {
        if ($value >= 0 && $value <= 100) {
            // percent * 21474837 / 2147483647 ≈ percent * 0.01
            ini_set('session.gc_probability', floor($value * 21474836.47));
            ini_set('session.gc_divisor', 2147483647);
        } else {
            throw new InvalidParamException('GCProbability must be a value between 0 and 100.');
        }
    }

    
    public function getUseTransparentSessionID()
    {
        return ini_get('session.use_trans_sid') == 1;
    }

    
    public function setUseTransparentSessionID($value)
    {
        ini_set('session.use_trans_sid', $value ? '1' : '0');
    }

    
    public function getTimeout()
    {
        return (int) ini_get('session.gc_maxlifetime');
    }

    
    public function setTimeout($value)
    {
        ini_set('session.gc_maxlifetime', $value);
    }

    
    public function getCount()
    {
        $this->open();
        return count($_SESSION);
    }

    
    public function count()
    {
        return $this->getCount();
    }

    
    public function get($key, $defaultValue = null)
    {
        $this->open();
        return isset($_SESSION[$key]) ? $_SESSION[$key] : $defaultValue;
    }

    
    public function set($key, $value)
    {
        $this->open();
        $_SESSION[$key] = $value;
    }

    
    public function remove($key)
    {
        $this->open();
        if (isset($_SESSION[$key])) {
            $value = $_SESSION[$key];
            unset($_SESSION[$key]);

            return $value;
        } else {
            return null;
        }
    }

    
    public function removeAll()
    {
        $this->open();
        foreach (array_keys($_SESSION) as $key) {
            unset($_SESSION[$key]);
        }
    }

    
    public function has($key)
    {
        $this->open();
        return isset($_SESSION[$key]);
    }



    
    public function hasFlash($key)
    {
        return $this->getFlash($key) !== null;
    }

    
    public function offsetExists($offset)
    {
        $this->open();

        return isset($_SESSION[$offset]);
    }

    
    public function offsetGet($offset)
    {
        $this->open();

        return isset($_SESSION[$offset]) ? $_SESSION[$offset] : null;
    }

    
    public function offsetSet($offset, $item)
    {
        $this->open();
        $_SESSION[$offset] = $item;
    }

    
    public function offsetUnset($offset)
    {
        $this->open();
        unset($_SESSION[$offset]);
    }
}
*/