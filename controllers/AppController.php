<?php

class AppController extends BaseController
{
    
    public function before_index(&$params)
    {
        
        $vars = [
            'menus'         => Helper::getMenus(join('.', $params)),
            'urlRoot'       => Flight::get('urlRoot'),
            'urlLangRoot'   => Flight::get('urlLangRoot')
        ];
        
        Flight::render('layouts/header', $vars, 'header_content');
        Flight::render('layouts/footer', [], 'footer_content');
        Flight::render('layouts/wrapper/navigation_wrapper', [], 'navigation_wrapper');
        Flight::render('layouts/wrapper/copyright_wrapper', [], 'copyright_wrapper');
    }

    public function index()
    {
        $args = func_get_args();
        
        if(isset($args[0]) && $args[0]=='translate')
        {
            $request = Flight::request();
            $key = $request->query->key;
            if($key != Flight::get('key'))
            {
                Flight::error(new Exception('', 401), 'La clave proporcinada es incorrecta');
            }
        }
        
        $pages = ['footer_wrapper','header_wrapper','intro_wrapper','wrapper'];
        
        Helper::reverseTranslate($args);
        $folder = join("/", $args);
        foreach( $pages as $page)
        {
            if($folder && file_exists(Flight::view()->path."/layouts/$folder/$page.php"))
            {
                Flight::render("layouts/$folder/$page", ['args' => $args], $page);
            }
            else
            {
                if($page == 'wrapper' && $folder!='')
                {
                    Flight::notFound();
                }
                else
                {
                    Flight::render("layouts/wrapper/$page", ['args' => $args], $page);
                }
            }
        }
        
        
    }
    
    public function after_index()
    {
        Flight::render('app/index', []);
    }
    
    public function translate()
    {
        $request = Flight::request();
        $key = $request->query->key;
        if($key != Flight::get('key'))
        {
            Flight::halt(401, 'La clave proporcinada es incorrecta.');
        }
        $data = $request->data;
        
        $translation = Flight::translation();
        foreach ($data as $item)
        {
            $k = $item['key'];
            $v = $item['value'];
            $translation->set($k, $v);
        }
        Flight::redirect($request->url);
    }
    
    public function contacto()
    {
        $request = Flight::request();
        
        $data = $request->data;
        $name = Helper::cleanInput($data->name);
        if(!$name) Flight::error(new Exception('', 401), 'Debe ingresar un nombre');
        
        $from = Helper::cleanInput($data->email);
        if(!$from) Flight::error(new Exception('', 401), 'Debe ingresar un email');
        if (!filter_var($from, FILTER_VALIDATE_EMAIL)) Flight::error(new Exception('', 401), 'El email ingresado no es válido');
        
        $message = Helper::cleanInput($data->message);
        if(!$message) Flight::error(new Exception('', 401), 'Debe ingresar un mensaje');
        
        $file = ($request->files)?$request->files['file']:NULL;
        
        $email = Swift_Message::newInstance()
                ->setReplyTo([$from => $name])
                ->setFrom(['no-reply@madererapizarro.com' => $name])
                ->setSubject("Contacto desde página web madererapizarro.com")
                ->setBody("$name <$from>:\n$message")
                ;
        
        $to = Flight::params('email');
        
        $email->setTo($to);
        
        if($file)
        {
            $emailAttachPermit = Flight::get('emailAttach');
            if(in_array($file['type'], $emailAttachPermit))
            {
                $email->attach(Swift_Attachment::fromPath($file['tmp_name'], $file['type'])->setFilename($file['name']));
            }
            else
            {
                Flight::error(new Exception('', 401), 'El archivo que está intentando enviar, no está permido.');
            }
        }
        
        $transport = Swift_SmtpTransport::newInstance();
        $mailer = Swift_Mailer::newInstance($transport);
        try
        {
            $result = $mailer->send($email);
            Flight::json(['success' => $result>0], 200);
        }
        catch (Exception $ex)
        {
            
            Flight::error($ex, 'Ocurrió un error al enviar el mensaje, reinténtelo luego.');
        }
    }
}