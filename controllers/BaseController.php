<?php

class BaseController
{
    protected $class_name;
    protected $call_name;
    protected $call_args;
    
    public function __construct($class_name, $call_name, $call_args)
    {
        $this->class_name = $class_name;
        $this->call_name = $call_name;
        $this->call_args = $call_args;
        
        $name = $class_name . "_" . $call_name;

        
        Flight::before($name, array($this, "before_action"));
        
        if(is_callable([$this, "before_".$call_name]))
            Flight::before($name, [$this, "before_".$call_name]);
        if(is_callable([$this, "after_".$call_name]))
            Flight::after($name, [$this, "after_".$call_name]);
        
        Flight::after($name, array($this, "after_action"));
    }
    
    public function before_action(&$params, &$output)
    {
        $i18n = Flight::get('i18n');
        
        if( isset($params[0]) && (array_key_exists($params[0],$i18n)) )
        {
            Flight::set('lang', $params[0]);
            $urlRoot = Flight::get('urlLangRoot');
            Flight::set('urlLangRoot', $urlRoot. $params[0] ."/");
            
            Flight::translation()->setLang($params[0]);
            
            array_shift($params);
        }
        $params = array_filter($params);
    }
    
    public function after_action(&$params, &$output)
    {
        
    }
}