<?php

defined('FLIGHT_DEBUG') or define('FLIGHT_DEBUG', true);

require(__DIR__ . '/../vendor/autoload.php');
require(__DIR__ . '/../bootstrap.php');

Flight::start();