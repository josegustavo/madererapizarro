(function($) {

	$(function() {
		try {
			if (typeof _wpcf7 == 'undefined' || _wpcf7 === null)
				_wpcf7 = {};

			_wpcf7 = $.extend({ cached: 0 }, _wpcf7);

			_wpcf7.supportHtml5 = $.wpcf7SupportHtml5();

			$('div.wpcf7 > form').ajaxForm({
				beforeSubmit: function(formData, jqForm, options) {
					jqForm.wpcf7ClearResponseOutput();
					jqForm.find('img.ajax-loader').css({ visibility: 'visible' });
					return true;
				},
				beforeSerialize: function(jqForm, options) {
					jqForm.find('[placeholder].placeheld').each(function(i, n) {
						$(n).val('');
					});
					return true;
				},
				data: { '_wpcf7_is_ajax_call': 1 },
				dataType: 'json',
				success: function(data,status,xhr,form) {
                                    
                                    form.find('img.ajax-loader').css({ visibility: 'hidden' });
                                    if(data.success)
                                    {
                                        alert("Su mensaje fué enviado, nos estaremos comunicando en la dirección brindada a la brevedad. Gracias.");
                                        $(form).trigger("reset");
                                    }
                                    else
                                    {
                                        alert("Disculpe pero no se pudo enviar el mensaje, intente enviar un email.\nGracias");
                                    }
				},
				error: function(xhr, status, error, $form) {
                                    $form.find('img.ajax-loader').css({ visibility: 'hidden' });
                                    
                                    var message = (xhr && xhr.responseJSON && xhr.responseJSON.error && xhr.responseJSON.error.detail )?xhr.responseJSON.error.detail:"Ocurrió un error desconocido.";
                                    alert(message);
                                    //var e = $('<div class="ajax-error"></div>').text(message);
                                    //$form.after(e);
				}
			});

			$('div.wpcf7 > form').wpcf7InitForm();

		} catch (e) {
		}
	});

	$.fn.wpcf7InitForm = function() {
		return this.each(function(i, n) {
			var $f = $(n);

			if (_wpcf7.cached)
				$f.wpcf7OnloadRefill();

			$f.wpcf7ToggleSubmit();

			$f.find('.wpcf7-submit').wpcf7AjaxLoader();

			$f.find('.wpcf7-acceptance').click(function() {
				$f.wpcf7ToggleSubmit();
			});

			$f.find('.wpcf7-exclusive-checkbox').wpcf7ExclusiveCheckbox();

			$f.find('[placeholder]').wpcf7Placeholder();

			if (_wpcf7.jqueryUi && ! _wpcf7.supportHtml5.date) {
				$f.find('input.wpcf7-date[type="date"]').each(function() {
					$(this).datepicker({
						dateFormat: 'yy-mm-dd',
						minDate: new Date($(this).attr('min')),
						maxDate: new Date($(this).attr('max'))
					});
				});
			}

			if (_wpcf7.jqueryUi && ! _wpcf7.supportHtml5.number) {
				$f.find('input.wpcf7-number[type="number"]').each(function() {
					$(this).spinner({
						min: $(this).attr('min'),
						max: $(this).attr('max'),
						step: $(this).attr('step')
					});
				});
			}
		});
	};

	$.fn.wpcf7ExclusiveCheckbox = function() {
		return this.find('input:checkbox').click(function() {
			$(this).closest('.wpcf7-checkbox').find('input:checkbox').not(this).removeAttr('checked');
		});
	};

	$.fn.wpcf7Placeholder = function() {
		if (_wpcf7.supportHtml5.placeholder)
			return this;

		return this.each(function() {
			$(this).val($(this).attr('placeholder'));
			$(this).addClass('placeheld');

			$(this).focus(function() {
				if ($(this).hasClass('placeheld'))
					$(this).val('').removeClass('placeheld');
			});

			$(this).blur(function() {
				if ('' == $(this).val()) {
					$(this).val($(this).attr('placeholder'));
					$(this).addClass('placeheld');
				}
			});
		});
	};

	$.fn.wpcf7AjaxLoader = function() {
		return this.each(function() {
			var loader = $('<img class="ajax-loader" />')
				.attr({ src: _wpcf7.loaderUrl, alt: _wpcf7.sending })
				.css('visibility', 'hidden');

			$(this).after(loader);
		});
	};

	$.fn.wpcf7ToggleSubmit = function() {
		return this.each(function() {
			var form = $(this);
			if (this.tagName.toLowerCase() != 'form')
				form = $(this).find('form').first();

			if (form.hasClass('wpcf7-acceptance-as-validation'))
				return;

			var submit = form.find('input:submit');
			if (! submit.length) return;

			var acceptances = form.find('input:checkbox.wpcf7-acceptance');
			if (! acceptances.length) return;

			submit.removeAttr('disabled');
			acceptances.each(function(i, n) {
				n = $(n);
				if (n.hasClass('wpcf7-invert') && n.is(':checked')
				|| ! n.hasClass('wpcf7-invert') && ! n.is(':checked'))
					submit.attr('disabled', 'disabled');
			});
		});
	};

	$.fn.wpcf7NotValidTip = function(message) {
		return this.each(function() {
			var into = $(this);
			into.append('<span class="wpcf7-not-valid-tip">' + message + '</span>');
			$('span.wpcf7-not-valid-tip').mouseover(function() {
				$(this).fadeOut('fast');
			});
			into.find(':input').mouseover(function() {
				into.find('.wpcf7-not-valid-tip').not(':hidden').fadeOut('fast');
			});
			into.find(':input').focus(function() {
				into.find('.wpcf7-not-valid-tip').not(':hidden').fadeOut('fast');
			});
		});
	};

	$.fn.wpcf7OnloadRefill = function() {
		return this.each(function() {
			var url = $(this).attr('action');
			if (0 < url.indexOf('#'))
				url = url.substr(0, url.indexOf('#'));

			var id = $(this).find('input[name="_wpcf7"]').val();
			var unitTag = $(this).find('input[name="_wpcf7_unit_tag"]').val();

			$.getJSON(url,
				{ _wpcf7_is_ajax_call: 1, _wpcf7: id, _wpcf7_request_ver: $.now() },
				function(data) {
					if (data && data.captcha)
						$('#' + unitTag).wpcf7RefillCaptcha(data.captcha);

					if (data && data.quiz)
						$('#' + unitTag).wpcf7RefillQuiz(data.quiz);
				}
			);
		});
	};

	$.fn.wpcf7RefillCaptcha = function(captcha) {
		return this.each(function() {
			var form = $(this);

			$.each(captcha, function(i, n) {
				form.find(':input[name="' + i + '"]').clearFields();
				form.find('img.wpcf7-captcha-' + i).attr('src', n);
				var match = /([0-9]+)\.(png|gif|jpeg)$/.exec(n);
				form.find('input:hidden[name="_wpcf7_captcha_challenge_' + i + '"]').attr('value', match[1]);
			});
		});
	};

	$.fn.wpcf7RefillQuiz = function(quiz) {
		return this.each(function() {
			var form = $(this);

			$.each(quiz, function(i, n) {
				form.find(':input[name="' + i + '"]').clearFields();
				form.find(':input[name="' + i + '"]').siblings('span.wpcf7-quiz-label').text(n[0]);
				form.find('input:hidden[name="_wpcf7_quiz_answer_' + i + '"]').attr('value', n[1]);
			});
		});
	};

	$.fn.wpcf7ClearResponseOutput = function() {
		return this.each(function() {
			$(this).find('div.wpcf7-response-output').hide().empty().removeClass('wpcf7-mail-sent-ok wpcf7-mail-sent-ng wpcf7-validation-errors wpcf7-spam-blocked');
			$(this).find('span.wpcf7-not-valid-tip').remove();
			$(this).find('img.ajax-loader').css({ visibility: 'hidden' });
			$(this).parent().find('.ajax-error').remove();
			$(this).parent().find('.ajax-success').remove();
		});
	};

	$.fn.wpcf7FillResponseOutput = function(message) {
		return this.each(function() {
			$(this).find('div.wpcf7-response-output').append(message).slideDown('fast');
		});
	};

	$.wpcf7SupportHtml5 = function() {
		var features = {};
		var input = document.createElement('input');

		features.placeholder = 'placeholder' in input;

		var inputTypes = ['email', 'url', 'tel', 'number', 'range', 'date'];

		$.each(inputTypes, function(index, value) {
			input.setAttribute('type', value);
			features[value] = input.type !== 'text';
		});

		return features;
	};

})(jQuery);