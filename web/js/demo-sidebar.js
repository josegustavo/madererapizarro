jQuery(document).ready(function() {
	jQuery(".stylespanel-toggle").click(function(e) {
		e.preventDefault();
		jQuery(".stylespanel-content").slideToggle();
	});
	jQuery(".stylespanel-content").hide();

	jQuery(".colorspanel-toggle").toggle(
		function() {
			var $panel = jQuery(this).prev(".colorspanel-content");
			$panel.animate({ left: 0 });
		},
		function() {
			var $panel = jQuery(this).prev(".colorspanel-content");
			$panel.animate({ left: -180 });
		}
	);
});
